# Fight Simulator Cli

[[_TOC_]]

---

## Description

This project aim to simulate chances to win a fight in crypto raider.

## Usage

```bash
$ ./bin/fight-simulator-cli fight --help
fight-simulator-cli fight <fighterA> <fighterB>

Simulate a fight between two fighters

Positionals:
  fighterA  Can be:
            - a mob name (hogger,hoggerHeroic,faune,fauneHeroic)
            - a Raider ID
            - a mock of a raider X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc)                            [string] [required]
  fighterB  Can be:
            - a mob name (hogger,hoggerHeroic,faune,fauneHeroic)
            - a Raider ID
            - a mock of a raider X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc)                            [string] [required]

Options:
      --version         Show version number                                                                    [boolean]
  -n, --noninteractive  Will skip all questions                                               [boolean] [default: false]
  -h, --help            Show help                                                                              [boolean]
```

```bash
$ ./bin/fight-simulator-cli mfight --help
fight-simulator-cli mfight <fighterA> <fighterB> <simCount>

Simulate multiple fights between two fighters

Positionals:
  fighterA  Can be:
            - a mob name (hogger,hoggerHeroic,faune,fauneHeroic)
            - a Raider ID
            - a mock of a raider X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc)                            [string] [required]
  fighterB  Can be:
            - a mob name (hogger,hoggerHeroic,faune,fauneHeroic)
            - a Raider ID
            - a mock of a raider X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc)                            [string] [required]
  simCount  The number of simulation                                                                 [number] [required]

Options:
      --version         Show version number                                                                    [boolean]
  -n, --noninteractive  Will skip all questions                                               [boolean] [default: false]
  -h, --help            Show help                                                                              [boolean]
```

## Support

You can get support on our [official discord](https://discord.gg/ChBxxrqajc), also on [Crypto Raider official server](https://discord.gg/kDzjdrt3m7) and [For Loot And Glory official server](https://discord.gg/gz3W3Q2FwF).

If you are a developer or have question about the development, you can create an [issue here](https://gitlab.com/crypto-raider-tools/fight-simulator-cli/-/issues/new).

## Contributing as a user

- Thanks us on discord ;) or by creating an [issue here](https://gitlab.com/crypto-raider-tools/fight-simulator-cli/-/issues/new)
- Donate us to `0xD497a4A178a347d401de316fFA501f2F285D8e72` (MetaMask)

## Contributing as a developer

- Feel free to create a merge request, no need to ask if you can ;)
  - "_Why my commit are refused?_" See https://juhani.gitlab.io/go-semrel-gitlab/commit-message/
- See below to start working on the project

### Installation

Requirements:

- npm >= 6.14.14
- node >= 14.17.5

```bash
npm install
```

### Usage

```bash
npm run clean
npm run lint
npm run compile
npm run package
# alternatively, npm run all
./bin/fight-simulator-cli --help
```

## Delivery

```
npm version <M.m.p> --tag-version-prefix=""
git push && git push --tags
```

## Authors and acknowledgment

- @le.storm1er / `storm1er#0376`
- @benjamin.silvert / `OSi#6927`

## License

See https://gitlab.com/crypto-raider-tools/fight-simulator-cli/-/blob/main/LICENSE  
See https://choosealicense.com/licenses/gpl-2.0/
