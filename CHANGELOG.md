# CHANGELOG

<!--- next entry here -->

## 3.1.1
2022-03-04

### Fixes

- Updated compute package (eb1b1e149780cdeb4a5bf378e80577c010c8849e)

## 3.1.0

2022-02-23

### Features

- Add finger stuff (2899c87123d729203cabab5789d714a9c40c2922)

## 3.0.4

2022-02-16

### Fixes

- Added robber mob (c592b0c437d71ed7f1765f70f98f06c5e30a2460)
