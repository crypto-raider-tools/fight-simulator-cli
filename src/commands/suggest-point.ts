import type { Arguments, CommandBuilder } from 'yargs';
import { Fight } from '../lib/fight';
import { FighterInterface } from '../lib/fighters/fighter';
import { fromStringToFighter } from '../lib/fighters/fighterUtils';
import { FighterStatsName } from '../lib/fighters/raider/raider';
import { displayAllLog, displayDebugLog, log } from '../lib/logger';
import { askGear, fromServerInputToFighter } from './fight';
import * as cliProgress from 'cli-progress';
import { FighterStatsEssentials, isKnickknackName, MobBuildName, mobBuilds } from '@crypto-raider-tools/computation';

type Options = {
  noninteractive: boolean;
  fighter: string;
  availablePoints: number;
  simCount: number;
};

export const command: string = 'suggest-point <fighter> <availablePoints> <simCount>';
export const desc: string = 'Will give you the best place to spend your point.';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs
    .boolean('noninteractive')
    .alias('noninteractive', 'n')
    .default('noninteractive', false)
    .describe('noninteractive', 'Will skip all question')

    .positional('fighter', { type: 'string', demandOption: true })
    .describe(
      'fighter',
      `Can be:\n\t- a mob name (${Object.keys(mobBuilds).join(
        ','
      )})\n\t- a Raider ID\n\t- a mock of a raider X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc,Lvl)`
    )
    .positional('availablePoints', { type: 'number', demandOption: true })
    .describe('availablePoints', 'Point quantity to be spent')
    .positional('simCount', { type: 'number', demandOption: true })
    .describe('simCount', 'The number of simulation per suggestion');

let avoidStackExplosion = 0;

const getNextPossibility = async (
  stats: FighterStatsEssentials,
  availablePoints: number,
  offset: number = 0
): Promise<Array<FighterStatsEssentials>> => {
  if (availablePoints === 0) {
    return Promise.resolve([]);
  }

  return new Promise((resolve) => {
    const method = avoidStackExplosion > 1000 ? setTimeout : (cb: Function) => cb();
    avoidStackExplosion > 1000 ? (avoidStackExplosion = 0) : avoidStackExplosion++;
    method(async () => {
      const statNames: Array<FighterStatsName> = ['agility', 'charm', 'intelligence', 'luck', 'strength', 'wisdom'];
      let morePossibility: Array<FighterStatsEssentials> = [];
      for (let index = offset; index < statNames.length; index++) {
        const statName = statNames[index];
        const newBaseStats: FighterStatsEssentials = {
          ...stats,
          stats: { ...stats.stats, [statName]: stats.stats[statName] + 1 },
        };
        if (availablePoints === 1) {
          morePossibility.push(newBaseStats);
        }
        morePossibility = [...morePossibility, ...(await getNextPossibility(newBaseStats, availablePoints - 1, index))];
      }
      resolve(morePossibility);
    }, 0);
  });
};

const getAllPossibilities = async (
  fighter: FighterInterface,
  availablePoints: number
): Promise<Array<FighterStatsEssentials>> => {
  if (!fighter.isRaider()) {
    throw new Error('Only raider can execute this command.');
  }
  if (!fighter.stats) {
    throw new Error('Raider must be ready.');
  }
  return await getNextPossibility(fighter.stats, availablePoints);
};

export type SuggestPointOutput = FighterStatsEssentials & { winRate?: number };

export const startSuggestPoint = async (
  fighter: FighterInterface,
  availablePoints: number,
  simCount: number
): Promise<SuggestPointOutput> => {
  log('SuggestPoint', `Computing possibilities...`, 'info');
  const allPossibilities: Array<SuggestPointOutput> = await getAllPossibilities(fighter, availablePoints);
  const mobNames: Array<MobBuildName> = ['fauneHeroic', 'robberHeroic'];
  const simulationCount = allPossibilities.length * simCount * mobNames.length;
  log('SuggestPoint', `Starting ${simulationCount} simulations`, 'info');
  displayAllLog(false);

  let currentSimCount = 0;
  const progress = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic);
  progress.start(simulationCount, currentSimCount);

  for (let index = 0; index < allPossibilities.length; index++) {
    const possibility = allPossibilities[index];
    const mobResult: Array<number> = [];
    for (let mobIndex = 0; mobIndex < mobNames.length; mobIndex++) {
      const mockRaider = fromServerInputToFighter({ level: 1, stats: possibility.stats });
      const mobName = mobNames[mobIndex];
      const mob = fromStringToFighter(mobName);
      await mockRaider.fromInitToLoading();
      await mockRaider.fromLoadingToReady();
      await mob.fromInitToLoading();
      await mob.fromLoadingToReady();
      let preCheckSimCount = 0;
      if (simCount >= 10) {
        preCheckSimCount = Math.floor(simCount * 0.3);
        const preResult = await startMFight(preCheckSimCount, mockRaider, mob);
        if (preResult.fighterAWinCount === preCheckSimCount) {
          mobResult.push(1);
          currentSimCount += simCount;
          continue;
        }
        if (preResult.fighterAWinCount === 0) {
          mobResult.push(0);
          currentSimCount += simCount;
          continue;
        }
      }
      const result = await startMFight(simCount - preCheckSimCount, mockRaider, mob);
      mobResult.push(result.fighterAWinCount / (result.fighterAWinCount + result.fighterBWinCount));
      currentSimCount += simCount;
    }
    progress.update(currentSimCount);
    allPossibilities[index].winRate = mobResult.reduce((previous, current) => previous + current, 0) / mobResult.length;
  }

  progress.stop();
  displayAllLog(true);
  const bestPossibility = allPossibilities.reduce((previous, current) => {
    if (!current.winRate) return previous;
    if (!previous) return current;
    if (!previous.winRate || previous.winRate < current.winRate) return current;
    return previous;
  });
  log('SuggestPoint', `Best possibility found: ${JSON.stringify(bestPossibility)}`, 'info');
  return Promise.resolve(bestPossibility);
};

export const handler = (argv: Arguments<Options>): void => {
  displayDebugLog(false);
  const { noninteractive, fighter, availablePoints, simCount } = argv;

  if (noninteractive) {
    let fighterObject = fromStringToFighter(fighter);
    if (!fighterObject.isRaider()) {
      throw new Error('Only raider can execute this command.');
    }
    fighterObject
      .fromInitToLoading()
      .then(() => fighterObject.fromLoadingToReady())
      .then(() => {
        startSuggestPoint(fighterObject, availablePoints, simCount);
      });
    return;
  }

  askGear(fighter, null).then((gears) => {
    let fighterObject = fromStringToFighter(fighter);
    if (!fighterObject.isRaider()) {
      throw new Error('Only raider can execute this command.');
    }
    fighterObject
      .fromInitToLoading()
      .then(() => fighterObject.fromLoadingToReady())
      .then(() => {
        if (fighterObject.isRaider()) {
          const knickknack = isKnickknackName(gears.AK)
            ? {
                name: gears.AK,
              }
            : null;
          return fighterObject.equip(gears.A1 ?? null, gears.A2 ?? null, knickknack, gears.AF ?? null);
        }
        return Promise.resolve(fighterObject);
      })
      .then(() => {
        startSuggestPoint(fighterObject, availablePoints, simCount);
      });
  });
};

type MFightOutput = {
  fighterAWinCount: number;
  fighterBWinCount: number;
};

const startMFight = async (
  simCount: number,
  fighterObjectA: FighterInterface,
  fighterObjectB: FighterInterface
): Promise<MFightOutput> => {
  if (simCount > 1000) {
    log('Result', `Too much simCount ${simCount}`, 'info');
    return Promise.resolve({
      fighterAWinCount: 0,
      fighterBWinCount: 0,
    });
  }

  const fights: Array<Fight> = [];

  let currentSimCount = 0;
  let fighterAWinCount = 0;
  let fighterBWinCount = 0;
  while (currentSimCount < simCount) {
    fights.push(new Fight(fighterObjectA, fighterObjectB));
    await fights[currentSimCount].simulate();
    fighterAWinCount += fights[currentSimCount].hasFighterAWon() ? 1 : 0;
    fighterBWinCount += fights[currentSimCount].hasFighterAWon() ? 0 : 1;
    fighterObjectA.reset();
    fighterObjectB.reset();
    currentSimCount++;
  }
  log('Result', `${fighterObjectA.name} won ${fighterAWinCount} time(s)`, 'info');
  log('Result', `${fighterObjectB.name} won ${fighterBWinCount} time(s)`, 'info');
  return Promise.resolve({
    fighterAWinCount,
    fighterBWinCount,
  });
};
