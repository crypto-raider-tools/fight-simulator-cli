import type { CommandBuilder } from 'yargs';
import express, { Request, Response } from 'express';
import { log } from '../lib/logger';
import { serveHandler as mFightServeHandler } from './mfight';
import { isStats, KnickknackName, Stats } from '@crypto-raider-tools/computation';

export const command: string = 'serve';
export const desc: string = 'Start a server, call http://HOST:3000/help to see the help.';

export const builder: CommandBuilder<{}, {}> = (yargs) => yargs;

export type FighterInput = {
  id?: number | string;
  stats?: Stats;
  gears?: Array<Stats>;
  level?: number;
  knickknack?: {
    name: KnickknackName;
  };
};

export type FightInput = {
  fighterA: FighterInput;
  fighterB: FighterInput;
};

export type MFightInput = {
  fighterA: FighterInput;
  fighterB: FighterInput;
  simCount: number;
};

export const isFighterStatsInput = (data: any): data is Stats => {
  if ('object' !== typeof data) return false;
  if ('number' !== typeof data?.strength) return false;
  if ('number' !== typeof data?.intelligence) return false;
  if ('number' !== typeof data?.agility) return false;
  if ('number' !== typeof data?.wisdom) return false;
  if ('number' !== typeof data?.charm) return false;
  if ('number' !== typeof data?.luck) return false;
  return true;
};

export const isMFightInput = (data: any): data is MFightInput => {
  return typeof data.simCount === 'number' && isFightInput(data);
};

export const isFightInput = (data: any): data is FightInput => {
  const checkFighter = (data: any): data is FighterInput => {
    if (data.gears) {
      if (!data.gears?.length) return false;
      for (let index = 0; index < data.gears.length; index++) {
        const gear = data.gears[index];
        if (!isStats(gear)) return false;
      }
    }
    if (data?.id && data?.stats) return false;
    if (data.id) return ['number', 'string'].includes(typeof data.id);
    if (data.stats) return isFighterStatsInput(data.stats);
    return false;
  };

  if ('object' !== typeof data) return false;
  if (!data.fighterA || !data.fighterB) {
    return false;
  }
  return checkFighter(data.fighterA) && checkFighter(data.fighterB);
};

const isAuthenticated = (req: Request) => {
  if (!process.env.AUTHBEARER || process.env.AUTHBEARER === '') {
    return true;
  }
  if (req.headers.authorization && req.headers.authorization === process.env.AUTHBEARER) {
    return true;
  }
  return false;
};

export const handler = (): void => {
  const app = express();
  const port = 3000;

  const fightExampleA: FightInput = {
    fighterA: {
      id: 1234,
      gears: [
        {
          strength: 1,
          intelligence: 2,
          agility: 3,
          wisdom: 4,
          charm: 5,
          luck: 6,
        },
        {
          strength: 1,
          intelligence: 2,
          agility: 3,
          wisdom: 4,
          charm: 5,
          luck: 6,
        },
        {
          strength: 0,
          intelligence: 0,
          agility: 4,
          wisdom: 0,
          charm: 0,
          luck: 0,
        },
      ],
    },
    fighterB: { id: 'hoggerHeroic' },
  };

  const fightExampleB: FightInput = {
    fighterA: {
      level: 2,
      stats: {
        strength: 8,
        intelligence: 9,
        agility: 10,
        wisdom: 11,
        charm: 12,
        luck: 13,
      },
      knickknack: {
        name: 'Bash',
      },
    },
    fighterB: { id: 'faune' },
  };

  app.use(express.json());

  // @ts-expect-error
  app.get('/help', (req, res) => {
    log('GET', `/help`, 'info');
    res.send(`<pre>
Crypto Raider Tools: Fight Simulator CLI as server

GET /help: This help page.

POST /mfight: Simulate multiple fights
Usage: Method POST with this payload:
---------
${JSON.stringify({ simCount: 100, ...fightExampleA }, undefined, 2)}
---------
${JSON.stringify({ simCount: 100, ...fightExampleB }, undefined, 2)}
---------
    </pre>`);
  });

  app.post('/mfight', (req: Request, res: Response) => {
    if (!isAuthenticated(req)) {
      res.status(403).send({ status: 403, reason: 'Invalid Auth' });
      return;
    }
    const input = req.body;
    if (!isMFightInput(input)) {
      res.status(400).send({ status: 400, reason: 'Invalid Payload' });
      log('POST', `/fight 400`, 'info');
      return;
    }
    // Handle Auth
    log('POST', `/mfight`, 'info');
    mFightServeHandler(input, res);
  });

  app.listen(port, () => {
    log('Server', `Listening at http://localhost:${port}`, 'info');
  });
};
