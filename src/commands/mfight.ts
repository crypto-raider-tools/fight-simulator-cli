import { isKnickknackName, mobBuilds } from '@crypto-raider-tools/computation';
import { Response } from 'express';
import type { Arguments, CommandBuilder } from 'yargs';
import { Fight } from '../lib/fight';
import { FighterInterface } from '../lib/fighters/fighter';
import { fromStringToFighter } from '../lib/fighters/fighterUtils';
import { displayDebugLog, log } from '../lib/logger';
import { askGear, fromServerInputToFighter } from './fight';
import { MFightInput } from './serve';

type Options = {
  noninteractive: boolean;
  fighterA: string;
  fighterB: string;
  simCount: number;
};

export const command: string = 'mfight <fighterA> <fighterB> <simCount>';
export const desc: string = 'Simulate multiple fights between two fighters';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs
    .boolean('noninteractive')
    .alias('noninteractive', 'n')
    .default('noninteractive', false)
    .describe('noninteractive', 'Will skip all question')

    .positional('fighterA', { type: 'string', demandOption: true })
    .positional('fighterB', { type: 'string', demandOption: true })
    .describe(
      ['fighterA', 'fighterB'],
      `Can be:\n\t- a mob name (${Object.keys(mobBuilds).join(
        ','
      )})\n\t- a Raider ID\n\t- a mock of a raider X,X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc,Lvl)`
    )
    .positional('simCount', { type: 'number', demandOption: true })
    .describe('simCount', 'The number of simulation');

export const handler = (argv: Arguments<Options>): void => {
  displayDebugLog(false);
  const { fighterA, fighterB, noninteractive, simCount } = argv;

  if (simCount > 1000) {
    throw new Error("Damn, that's a lot of simulation to do...");
  }

  if (noninteractive) {
    let fighterObjectA = fromStringToFighter(fighterA);
    let fighterObjectB = fromStringToFighter(fighterB);
    Promise.all([
      fighterObjectA.fromInitToLoading().then((fighterObjectA) => fighterObjectA.fromLoadingToReady()),
      fighterObjectB.fromInitToLoading().then((fighterObjectB) => fighterObjectB.fromLoadingToReady()),
    ]).then(() => {
      startMFight(simCount, fighterObjectA, fighterObjectB);
    });
    return;
  }

  askGear(fighterA, fighterB).then((gears) => {
    let fighterObjectA = fromStringToFighter(fighterA);
    let fighterObjectB = fromStringToFighter(fighterB);
    Promise.all([
      fighterObjectA
        .fromInitToLoading()
        .then((fighterObjectA) => fighterObjectA.fromLoadingToReady())
        .then((fighterObjectA) => {
          if (fighterObjectA.isRaider()) {
            const knickknack = isKnickknackName(gears.AK)
              ? {
                  name: gears.AK,
                }
              : null;
            return fighterObjectA.equip(gears.A1 ?? null, gears.A2 ?? null, knickknack, gears.AF ?? null);
          }
          return Promise.resolve(fighterObjectA);
        }),
      fighterObjectB
        .fromInitToLoading()
        .then((fighterObjectB) => fighterObjectB.fromLoadingToReady())
        .then((fighterObjectB) => {
          if (fighterObjectB.isRaider()) {
            const knickknack = isKnickknackName(gears.BK)
              ? {
                  name: gears.BK,
                }
              : null;
            return fighterObjectB.equip(gears.B1 ?? null, gears.B2 ?? null, knickknack, gears.BF ?? null);
          }
          return Promise.resolve(fighterObjectB);
        }),
    ]).then(() => {
      startMFight(simCount, fighterObjectA, fighterObjectB);
    });
  });
};

type MFightOutputDetail = {
  fighterA: {
    remainingLife: number;
    damagePerRound: number;
  };
  fighterB: {
    remainingLife: number;
    damagePerRound: number;
  };
};

type MFightOutput = {
  fighterAWinCount: number;
  fighterBWinCount: number;
  fighterAAverage: {
    remainingLife: number;
    damagePerSim: number;
  };
  fighterBAverage: {
    remainingLife: number;
    damagePerSim: number;
  };
  fightsDetails?: Array<MFightOutputDetail>;
};

const startMFight = async (
  simCount: number,
  fighterObjectA: FighterInterface,
  fighterObjectB: FighterInterface
): Promise<MFightOutput> => {
  log('MFight', `Starting Mfight`, 'info');
  if (simCount > 1000) {
    log('MFight', `Too much simCount ${simCount}`, 'info');
    return Promise.resolve({
      fighterAWinCount: 0,
      fighterBWinCount: 0,
      fighterAAverage: {
        damagePerSim: 0,
        remainingLife: 0,
      },
      fighterBAverage: {
        damagePerSim: 0,
        remainingLife: 0,
      },
    });
  }

  const fights: Array<Fight> = [];

  let currentSimCount = 0;
  let fighterAWinCount = 0;
  let fighterBWinCount = 0;
  let fighterAAverage = {
    damagePerSim: 0,
    remainingLife: 0,
  };
  let fighterBAverage = {
    damagePerSim: 0,
    remainingLife: 0,
  };
  let fightsDetails: undefined | Array<MFightOutputDetail> = simCount <= 5 ? [] : undefined;
  while (currentSimCount < simCount) {
    const fightDetail: undefined | MFightOutputDetail = fightsDetails
      ? {
          fighterA: {
            remainingLife: 0,
            damagePerRound: 0,
          },
          fighterB: {
            remainingLife: 0,
            damagePerRound: 0,
          },
        }
      : undefined;
    fights.push(new Fight(fighterObjectA, fighterObjectB));
    await fights[currentSimCount].simulate();
    fighterAWinCount += fights[currentSimCount].hasFighterAWon() ? 1 : 0;
    fighterBWinCount += fights[currentSimCount].hasFighterAWon() ? 0 : 1;
    fights[currentSimCount].rounds.forEach((round) => {
      round.hits.forEach((hit) => {
        const averageObject = hit.from.name === fighterObjectA.name ? fighterAAverage : fighterBAverage;
        averageObject.damagePerSim += hit.damageDealt;
        if (fightDetail) {
          const key = hit.from.name === fighterObjectA.name ? 'fighterA' : 'fighterB';
          fightDetail[key].damagePerRound += hit.damageDealt;
        }
      });
    });
    if (fightDetail) {
      fightDetail.fighterA.damagePerRound /= fights[currentSimCount].rounds.size;
      fightDetail.fighterB.damagePerRound /= fights[currentSimCount].rounds.size;
      fightDetail.fighterA.damagePerRound = parseFloat(fightDetail.fighterA.damagePerRound.toFixed(1));
      fightDetail.fighterB.damagePerRound = parseFloat(fightDetail.fighterB.damagePerRound.toFixed(1));
      fightDetail.fighterA.remainingLife = fighterObjectA.lifeTime?.health || 0;
      fightDetail.fighterB.remainingLife = fighterObjectB.lifeTime?.health || 0;
      if (fightsDetails) fightsDetails.push(fightDetail);
    }
    fighterAAverage.remainingLife += fighterObjectA.lifeTime?.health || 0;
    fighterBAverage.remainingLife += fighterObjectB.lifeTime?.health || 0;
    await fighterObjectA.reset();
    await fighterObjectB.reset();
    currentSimCount++;
  }
  fighterAAverage.damagePerSim /= currentSimCount;
  fighterBAverage.damagePerSim /= currentSimCount;
  fighterAAverage.damagePerSim = parseFloat(fighterAAverage.damagePerSim.toFixed(1));
  fighterBAverage.damagePerSim = parseFloat(fighterBAverage.damagePerSim.toFixed(1));
  fighterAAverage.remainingLife /= currentSimCount;
  fighterBAverage.remainingLife /= currentSimCount;
  fighterAAverage.remainingLife = parseFloat(fighterAAverage.remainingLife.toFixed(1));
  fighterBAverage.remainingLife = parseFloat(fighterBAverage.remainingLife.toFixed(1));
  log(
    'MFight',
    `Win count: ${fighterObjectA.name}(${fighterAWinCount}) and ${fighterObjectB.name}(${fighterBWinCount})`,
    'info'
  );
  return Promise.resolve({
    fighterAWinCount,
    fighterBWinCount,
    fighterAAverage,
    fighterBAverage,
    fightsDetails,
  });
};

export const serveHandler = (fightInput: MFightInput, res: Response): void => {
  displayDebugLog(false);
  log('Serve MFight', `Starting`, 'info');
  try {
    let fighterObjectA = fromServerInputToFighter(fightInput.fighterA);
    let fighterObjectB = fromServerInputToFighter(fightInput.fighterB);
    Promise.all([
      fighterObjectA
        .fromInitToLoading()
        .then((fighterObjectA) => fighterObjectA.fromLoadingToReady())
        .then((fighterObjectA) => {
          if (fighterObjectA.isRaider()) {
            return fighterObjectA.equip(
              fightInput.fighterA.gears?.[0] ?? null,
              fightInput.fighterA.gears?.[1] ?? null,
              fightInput.fighterA.knickknack ?? null,
              fightInput.fighterA.gears?.[2] ?? null
            );
          }
          return Promise.resolve(fighterObjectA);
        }),
      fighterObjectB
        .fromInitToLoading()
        .then((fighterObjectB) => fighterObjectB.fromLoadingToReady())
        .then((fighterObjectB) => {
          if (fighterObjectB.isRaider()) {
            return fighterObjectB.equip(
              fightInput.fighterB.gears?.[0] ?? null,
              fightInput.fighterB.gears?.[1] ?? null,
              fightInput.fighterB.knickknack ?? null,
              fightInput.fighterB.gears?.[2] ?? null
            );
          }
          return Promise.resolve(fighterObjectB);
        }),
    ])
      .then(() => startMFight(fightInput.simCount, fighterObjectA, fighterObjectB))
      .then((result) => {
        res.status(200).send(result);
      });
    log('Serve MFight', `Done`, 'info');
  } catch (error) {
    res.status(400).send({
      status: 400,
      // @ts-ignore
      message: typeof error === 'string' ? error : error.toString(),
    });
    // @ts-ignore
    log('Serve MFight', typeof error === 'string' ? error : error.toString(), 'info');
  }
  return;
};
