import { isKnickknackName, isMobBuildName, KnickknackName, mobBuilds, Stats } from '@crypto-raider-tools/computation';
import prompt from 'prompt';
import type { Arguments, CommandBuilder } from 'yargs';
import { Fight } from '../lib/fight';
import { FighterInterface } from '../lib/fighters/fighter';
import { fromStringToFighter } from '../lib/fighters/fighterUtils';
import { FighterInput } from './serve';

type Options = {
  noninteractive: boolean;
  fighterA: string;
  fighterB: string;
};

export const command: string = 'fight <fighterA> <fighterB>';
export const desc: string = 'Simulate a fight between two fighters';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
  yargs
    .boolean('noninteractive')
    .alias('noninteractive', 'n')
    .default('noninteractive', false)
    .describe('noninteractive', 'Will skip all question')

    .positional('fighterA', { type: 'string', demandOption: true })
    .positional('fighterB', { type: 'string', demandOption: true })
    .describe(
      ['fighterA', 'fighterB'],
      `Can be:\n\t- a mob name (${Object.keys(mobBuilds).join(
        ','
      )})\n\t- a Raider ID\n\t- a mock of a raider X,X,X,X,X,X,X (as Str,Int,Agi,Wis,Cha,Luc,Lvl)`
    );

export const gearFromString = (input: string): Stats => {
  const data = input.split(',');
  if (false === /^\d+,\d+,\d+,\d+,\d+,\d+$/.test(input)) {
    throw new Error(`Can't create a gear from this input: "${input}"`);
  }
  return {
    strength: parseInt(data[0]),
    intelligence: parseInt(data[1]),
    agility: parseInt(data[2]),
    wisdom: parseInt(data[3]),
    charm: parseInt(data[4]),
    luck: parseInt(data[5]),
  };
};

export const askGear = async (
  fighterAName: string,
  fighterBName: string | null
): Promise<Partial<Record<GearIndexes, Stats>>> => {
  const gearValidator = {
    pattern: /^\d+,\d+,\d+,\d+,\d+,\d+$/,
    required: false,
  };
  const knickknacksNames: Array<KnickknackName> = [
    'Bash',
    'Blind',
    'Escape Artist',
    'Ice Barrier',
    'Stone Skin',
    'Enflame',
    'Thorns',
  ];
  const knickknackValidator = {
    pattern: new RegExp(`^(${knickknacksNames.join('|')})$`),
    required: false,
  };

  let gearInputSchema: prompt.Schema = {
    properties: {},
  };

  if (!isMobBuildName(fighterAName)) {
    gearInputSchema = {
      properties: {
        A1: {
          description: `Please provide information for first gear of ${fighterAName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
        A2: {
          description: `Please provide information for second gear of ${fighterAName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
        AK: {
          description: `Please provide knickknack name of ${fighterAName} (${knickknacksNames.join(',')})`,
          ...knickknackValidator,
        },
        AF: {
          description: `Please provide information for finger gear of ${fighterAName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
      },
    };
  }

  if (fighterBName && !isMobBuildName(fighterBName)) {
    gearInputSchema = {
      properties: {
        B1: {
          description: `Please provide information for first gear of ${fighterBName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
        B2: {
          description: `Please provide information for second gear of ${fighterBName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
        BK: {
          description: `Please provide knickknack name of ${fighterBName} (${knickknacksNames.join(',')})`,
          ...knickknackValidator,
        },
        BF: {
          description: `Please provide information for finger gear of ${fighterBName} (format "str,int,agi,wis,cha,luc")`,
          ...gearValidator,
        },
        ...gearInputSchema.properties,
      },
    };
  }

  if (Object.keys(gearInputSchema.properties).length <= 0) {
    return new Promise((resolve) => {
      resolve({});
    });
  }

  // @ts-ignore;
  return prompt.get(gearInputSchema).then((data) => {
    let gears: Partial<Record<GearIndexes, Stats | KnickknackName>> = {};
    if ('string' === typeof data.A1 && '' !== data.A1) {
      gears.A1 = gearFromString(data.A1);
    }
    if ('string' === typeof data.A2 && '' !== data.A2) {
      gears.A2 = gearFromString(data.A2);
    }
    if ('string' === typeof data.AK && isKnickknackName(data.AK)) {
      gears.AK = data.AK;
    }
    if ('string' === typeof data.AF && '' !== data.AF) {
      gears.AF = gearFromString(data.AF);
    }
    if ('string' === typeof data.B1 && '' !== data.B1) {
      gears.B1 = gearFromString(data.B1);
    }
    if ('string' === typeof data.B2 && '' !== data.B2) {
      gears.B2 = gearFromString(data.B2);
    }
    if ('string' === typeof data.BK && isKnickknackName(data.BK)) {
      gears.BK = data.BK;
    }
    if ('string' === typeof data.BF && '' !== data.BF) {
      gears.BF = gearFromString(data.BF);
    }
    return gears;
  });
};

export type GearIndexes = 'A1' | 'A2' | 'AK' | 'AF' | 'B1' | 'B2' | 'BK' | 'BF';

export const handler = (argv: Arguments<Options>): void => {
  const { fighterA, fighterB, noninteractive } = argv;

  if (noninteractive) {
    let fighterObjectA = fromStringToFighter(fighterA);
    let fighterObjectB = fromStringToFighter(fighterB);
    Promise.all([
      fighterObjectA.fromInitToLoading().then((fighterObjectA) => fighterObjectA.fromLoadingToReady()),
      fighterObjectB.fromInitToLoading().then((fighterObjectB) => fighterObjectB.fromLoadingToReady()),
    ]).then(() => {
      const fight = new Fight(fighterObjectA, fighterObjectB);
      fight.simulate();
    });
    return;
  }

  askGear(fighterA, fighterB).then((gears) => {
    let fighterObjectA = fromStringToFighter(fighterA);
    let fighterObjectB = fromStringToFighter(fighterB);
    Promise.all([
      fighterObjectA
        .fromInitToLoading()
        .then((fighterObjectA) => fighterObjectA.fromLoadingToReady())
        .then((fighterObjectA) => {
          if (fighterObjectA.isRaider()) {
            const knickknack = isKnickknackName(gears.AK)
              ? {
                  name: gears.AK,
                }
              : null;
            return fighterObjectA.equip(gears.A1 ?? null, gears.A2 ?? null, knickknack, gears.AF ?? null);
          }
          return Promise.resolve(fighterObjectA);
        }),
      fighterObjectB
        .fromInitToLoading()
        .then((fighterObjectB) => fighterObjectB.fromLoadingToReady())
        .then((fighterObjectB) => {
          if (fighterObjectB.isRaider()) {
            const knickknack = isKnickknackName(gears.BK)
              ? {
                  name: gears.BK,
                }
              : null;
            return fighterObjectB.equip(gears.B1 ?? null, gears.B2 ?? null, knickknack, gears.BF ?? null);
          }
          return Promise.resolve(fighterObjectB);
        }),
    ]).then(() => {
      const fight = new Fight(fighterObjectA, fighterObjectB);
      fight.simulate();
    });
  });
};

export const fromServerInputToFighter = (fighterInput: FighterInput): FighterInterface => {
  let fighter: FighterInterface | undefined = undefined;
  if (fighterInput.id) {
    fighter = fromStringToFighter(fighterInput.id.toString());
  }
  if (fighterInput.stats && fighterInput.level) {
    fighter = fromStringToFighter(
      `${fighterInput.stats.strength},${fighterInput.stats.intelligence},${fighterInput.stats.agility},` +
        `${fighterInput.stats.wisdom},${fighterInput.stats.charm},${fighterInput.stats.luck},${fighterInput.level}`
    );
  }
  if ('undefined' === typeof fighter) {
    throw new Error('Unable to create a fighter from FighterInput');
  }
  return fighter;
};
