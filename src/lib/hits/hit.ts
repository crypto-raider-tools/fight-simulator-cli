import colors from 'colors/safe.js';
import { FighterDead, FighterEvaded, FighterFighting } from '../fighters/fighter';
import { log } from '../logger';
import { ActionObject, getHitResult } from './hitComputation';

export type HitLifetimeState = 'Init' | 'Miss' | 'Evade' | 'Hit' | 'Crit';

export interface HitInterface {
  status: HitLifetimeState;
  from: FighterFighting | FighterDead | FighterEvaded;
  to: FighterFighting | FighterDead | FighterEvaded;
  damageDealt: number;
}

/**
 * Return the damage dealt.
 */
function computeDamage(hit: Hit, isFirstRound: boolean = false): number {
  let actionObject: ActionObject = {
    actionTaken: 'Attack',
    caster: hit.from,
    targets: [hit.to],
  };

  let result = getHitResult(actionObject);
  if (isFirstRound && hit.from.isRaider() && hit.from.stats?.knickknack?.name) {
    actionObject.actionTaken = 'Spell';
    actionObject.spellName = hit.from.stats.knickknack.name;
    result = getHitResult(actionObject);
  }
  log(
    `Hit`,
    `${hit.from.name} ${colors.green(result.result)} (${colors.red(result.damage.toFixed(0))} dmg to target)`,
    'debug'
  );
  return result.damage;
}

function displayAffects(fighter: FighterFighting): void {
  for (let index = 0; index < fighter.lifeTime.affects.length; index++) {
    const affect = fighter.lifeTime.affects[index];
    log('Round', `${fighter.name} is ${colors.yellow(affect.affectName)}!`, 'debug');
  }
}

export class Hit {
  status: HitLifetimeState = 'Init';
  from: FighterFighting | FighterDead | FighterEvaded;
  to: FighterFighting | FighterDead | FighterEvaded;
  damageDealt: number = 0;
  constructor(from: FighterFighting, to: FighterFighting) {
    this.from = from;
    this.to = to;
    this.damageDealt = computeDamage(this);
    displayAffects(to);
    displayAffects(from);
    this.to.hit(this);
  }
}

export class FirstHit {
  status: HitLifetimeState = 'Init';
  from: FighterFighting | FighterDead | FighterEvaded;
  to: FighterFighting | FighterDead | FighterEvaded;
  damageDealt: number = 0;
  constructor(from: FighterFighting, to: FighterFighting) {
    this.from = from;
    this.to = to;
    this.damageDealt = computeDamage(this, true);
    displayAffects(to);
    displayAffects(from);
    this.to.hit(this);
  }
}
