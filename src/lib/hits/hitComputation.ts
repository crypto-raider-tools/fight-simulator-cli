import { KnickknackName } from '@crypto-raider-tools/computation';
import { FighterReady } from '../fighters/fighter';

type HitResult =
  | KnickknackName
  | 'EnflameMiss'
  | 'EnflameCrit'
  | 'EnflameHit'
  | 'BashMiss'
  | 'BashCrit'
  | 'BashHit'
  | 'MeleeHit'
  | 'MeleeMiss'
  | 'MeleeCrit';

export type HitObject = {
  result: HitResult;
  damage: number;
};

export type ActionObject = {
  actionTaken: 'Attack' | 'Spell';
  caster: FighterReady;
  targets: Array<FighterReady>;
  spellName?: KnickknackName;
};

export function getHitResult(actionObject: ActionObject): HitObject {
  let target = actionObject.targets[0],
    caster = actionObject.caster,
    spellName = actionObject.spellName,
    isCasterBlind = caster.lifeTime.affects.filter((value) => value.affectName === 'Blind').length > 0;
  if ('Attack' === actionObject.actionTaken) {
    let attackObject = createHitObject(caster, target, isCasterBlind),
      attackName = attackObject.result,
      attackDamage = attackObject.damage;
    return {
      result: attackName,
      damage: attackDamage,
    };
  }
  if ('Spell' === actionObject.actionTaken && spellName) {
    let affect, executedHit;
    if (!caster.isRaider() || null === caster.stats) {
      throw new Error("Can't spell using object without stats");
    }

    const strStat = { stat: 'strength', value: caster.stats.stats.strength };
    const intStat = { stat: 'intelligence', value: caster.stats.stats.intelligence };
    const agiStat = { stat: 'agility', value: caster.stats.stats.agility };
    //const wisStat = { stat: 'wisdom', value: caster.stats.stats.wisdom };
    //const chaStat = { stat: 'charm', value: caster.stats.stats.charm };
    //const lucStat = { stat: 'luck', value: caster.stats.stats.luck };
    const ratioForSortedStats = [1, 0.7, 0.05];
    //const secondaryStatsGroup = [wisStat, chaStat, lucStat];
    const primaryStatsGroupSorted = [strStat, intStat, agiStat].sort(function (e, t) {
      return t.value - e.value;
    });
    /*,
      secondaryStatsGroupSorted = secondaryStatsGroup.sort(function (e, t) {
        return t.value - e.value;
      });*/
    const strRatio = caster.stats.stats.strength * ratioForSortedStats[primaryStatsGroupSorted.indexOf(strStat)];
    const intRatio = caster.stats.stats.intelligence * ratioForSortedStats[primaryStatsGroupSorted.indexOf(intStat)];
    const agiRatio = caster.stats.stats.agility * ratioForSortedStats[primaryStatsGroupSorted.indexOf(agiStat)];
    /*const wisRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(wisStat)];
    const chaRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(chaStat)];
    const luckRatio = ratioForSortedStats[secondaryStatsGroupSorted.indexOf(lucStat)]; Not used  but i keep it just in case*/

    switch (spellName) {
      case 'Bash':
        let bashObject = createHitObject(caster, target, isCasterBlind, spellName),
          bashResult = bashObject.result,
          bashDamage = bashObject.damage;
        executedHit = {
          result: bashResult,
          damage: bashDamage,
        };
        break;
      case 'Ice Barrier':
        affect = caster.lifeTime.health + 50 + 1 * intRatio;
        caster.lifeTime.health = affect;
        executedHit = {
          result: spellName,
          damage: 0,
        };
        break;
      case 'Blind':
        target.lifeTime.affects.push({
          affectBuild: {
            hitChance: 0.75 * target.build.hitChance - 0.8 * caster.stats.stats.luck,
            evadeChance: target.build.evadeChance - caster.stats.stats.luck,
          },
          affectName: spellName,
          remainingTurns: 2,
        });
        caster.lifeTime.affects.push({
          affectBuild: {
            hitChance: caster.build.hitChance + caster.stats.stats.luck,
          },
          affectName: spellName,
          remainingTurns: 2,
        });
        executedHit = {
          result: spellName,
          damage: 0,
        };
        break;
      case 'Escape Artist':
        affect = caster.build.evadeChance + 30 + 0.25 * caster.stats.stats.charm + 0.25 * agiRatio;
        caster.lifeTime.affects.push({
          affectBuild: { evadeChance: affect },
          affectName: spellName,
          remainingTurns: 3,
        });
        executedHit = {
          result: spellName,
          damage: 0,
        };
        break;
      case 'Stone Skin':
        affect = 1.5 * caster.build.meleeResist + 0.5 * strRatio;
        caster.lifeTime.affects.push({
          affectBuild: { meleeResist: affect },
          affectName: spellName,
          remainingTurns: 2,
        });
        executedHit = {
          result: spellName,
          damage: 0,
        };
        break;
      case 'Enflame':
        let enflameObject = createHitObject(caster, target, isCasterBlind, spellName),
          enflameName = enflameObject.result,
          enflameDamage = enflameObject.damage;
        if (Math.random() * 100 >= 75) {
          target.lifeTime.affects.push({
            affectBuild: {},
            affectName: 'Enflame',
            remainingTurns: 3,
          });
        }
        executedHit = {
          result: enflameName,
          damage: enflameDamage,
        };
        break;
      case 'Thorns':
        affect = 1.3 * caster.build.meleeResist;
        caster.lifeTime.affects.push({
          affectBuild: { meleeResist: affect },
          affectName: spellName,
          remainingTurns: 3,
        });
        executedHit = {
          result: spellName,
          damage: 0,
        };
    }
    return executedHit;
  }
  throw TypeError('You can only give an attack action.');
}
function floating(min: number, max: number): number {
  let damage = min;
  const damageDiff = max - min;
  return damage + Math.random() * damageDiff;
}
function createHitObject(
  from: FighterReady,
  to: FighterReady,
  isBlind: boolean,
  spellName: 'Enflame' | 'Bash' | undefined = undefined
): HitObject {
  let damageToBeDealt = floating(
      from.build.minDamage * (spellName === 'Bash' ? 1.6 : 1),
      from.build.maxDamage * (spellName === 'Bash' ? 1.75 : 1)
    ),
    hitName: HitResult = 'MeleeHit';
  if (spellName || from.build.hitChance > parseFloat((Math.random() * 100).toFixed(2))) {
    if (to.build.evadeChance * (spellName === 'Bash' ? 0.9 : 1) > parseFloat((Math.random() * 100).toFixed(2))) {
      hitName = spellName === 'Bash' ? 'BashMiss' : spellName === 'Enflame' ? 'EnflameMiss' : 'MeleeMiss';
      damageToBeDealt = 0;
    } else {
      let meleeCrit = from.build.meleeCrit;
      isBlind && (meleeCrit = 0);
      spellName === 'Bash' && (meleeCrit = 1.1 * from.build.meleeCrit);
      spellName === 'Enflame' && (damageToBeDealt *= 1.25);
      isBlind && spellName === 'Bash' && (meleeCrit = 0.1 * from.build.meleeCrit);
      meleeCrit > parseFloat((Math.random() * 100).toFixed(2))
        ? ((damageToBeDealt *=
            floating(from.build.critDamageMultiplier - 0.2, from.build.critDamageMultiplier + 0.2) -
            to.build.critResist),
          (damageToBeDealt = Math.max(damageToBeDealt, from.build.maxDamage + 1)),
          (hitName = spellName === 'Bash' ? 'BashCrit' : spellName === 'Enflame' ? 'EnflameCrit' : 'MeleeCrit'))
        : (hitName = spellName === 'Bash' ? 'BashHit' : spellName === 'Enflame' ? 'EnflameHit' : 'MeleeHit');
    }
  } else {
    hitName = spellName === 'Bash' ? 'BashMiss' : spellName === 'Enflame' ? 'EnflameMiss' : 'MeleeMiss';
  }
  if (['EnflameMiss', 'BashMiss', 'MeleeMiss'].includes(hitName)) {
    damageToBeDealt = 0;
  }
  damageToBeDealt *= 1 - to.build.meleeResist / 100;
  return {
    result: hitName,
    damage: (damageToBeDealt = Math.floor(damageToBeDealt)),
  };
}
