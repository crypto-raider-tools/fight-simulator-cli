import { FighterFighting, FighterInterface } from './fighters/fighter';
import { isFighterFighting } from './fighters/fighterUtils';
import { FirstHit, Hit, HitInterface } from './hits/hit';
import { log } from './logger';
import colors from 'colors/safe.js';

export type RoundLifetimeState = 'Init' | 'FirstFighter' | 'SecondFighter' | 'Done';

export interface RoundInterface {
  firstFighter: FighterInterface;
  secondFighter: FighterInterface;
  currentState: RoundLifetimeState;
  hits: Array<HitInterface>;
  simulate(): boolean;
  fromInitToFirstFighter(): boolean;
  fromFirstFighterToSecondFighter(): boolean;
  fromFirstFighterToDone(): boolean;
  fromSecondFighterToDone(): boolean;
}

function displayHp(fighterA: FighterFighting, fighterB: FighterFighting) {
  log(
    'Round',
    `HP:    ${fighterA.name} ${colors.blue(fighterA.lifeTime.health.toFixed(0))} - ${colors.blue(
      fighterB.lifeTime.health.toFixed(0)
    )} ${fighterB.name}`,
    'debug'
  );
}

export class Round implements RoundInterface {
  firstFighter: FighterFighting;
  secondFighter: FighterFighting;
  currentState: RoundLifetimeState = 'Init';
  turn: number;
  hits: Array<HitInterface> = [];

  constructor(firstFighter: FighterFighting, secondFighter: FighterFighting, turn: number) {
    this.firstFighter = firstFighter;
    this.secondFighter = secondFighter;
    this.turn = turn;
  }

  simulate(): boolean {
    displayHp(this.firstFighter, this.secondFighter);
    return this.fromInitToFirstFighter();
  }

  fromInitToFirstFighter(): boolean {
    if (this.currentState !== 'Init') {
      throw new Error(`Transition from ${this.currentState} to FirstFighter is impossible`);
    }
    this.currentState = 'FirstFighter';
    const hit =
      this.turn === 1
        ? new FirstHit(this.firstFighter, this.secondFighter)
        : new Hit(this.firstFighter, this.secondFighter);
    this.hits.push(hit);
    if (!isFighterFighting(this.firstFighter) || !isFighterFighting(this.secondFighter)) {
      return this.fromFirstFighterToDone();
    }
    return this.fromFirstFighterToSecondFighter();
  }
  fromFirstFighterToSecondFighter(): boolean {
    if (this.currentState !== 'FirstFighter') {
      throw new Error(`Transition from ${this.currentState} to SecondFighter is impossible`);
    }
    if (this.hits.length != 1) {
      throw new Error("Can't transition from FirstFighter to SecondFighter if no hit has been done yet.");
    }
    this.currentState = 'SecondFighter';
    const hit = new Hit(this.secondFighter, this.firstFighter);
    this.hits.push(hit);
    return this.fromSecondFighterToDone();
  }
  fromFirstFighterToDone(): boolean {
    if (this.currentState !== 'FirstFighter') {
      throw new Error(`Transition from ${this.currentState} to Done is impossible`);
    }
    if (this.hits.length !== 1) {
      throw new Error("Can't transition from FirstFighter to Done if no hit has been done yet.");
    }
    if (this.secondFighter.status !== 'Dead' && this.secondFighter.status !== 'Evaded') {
      throw new Error("Can't transition from FirstFighter to Done if secondFighter not Dead/Evaded yet.");
    }
    this.currentState = 'Done';
    return true;
  }
  fromSecondFighterToDone(): boolean {
    if (this.currentState !== 'SecondFighter') {
      throw new Error(`Transition from ${this.currentState} to Done is impossible`);
    }
    if (this.hits.length !== 2) {
      throw new Error("Can't transition from SecondFighter to Done if only one hit has been done yet.");
    }
    this.currentState = 'Done';
    return true;
  }
}
