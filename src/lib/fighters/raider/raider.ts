import {
  computeRaiderBuild,
  FighterBuild,
  FighterStatsEssentials,
  KnickknackName,
  Stats,
} from '@crypto-raider-tools/computation';
import { HitInterface } from '../../hits/hit';
import { log } from '../../logger';
import {
  FighterDead,
  FighterEvaded,
  FighterFighting,
  FighterInit,
  FighterInterface,
  FighterLifetime,
  FighterLoading,
  FighterReady,
  FighterStatus,
} from '../fighter';
import {
  computeBuildWithAffects,
  computeAffectDamage,
  createFighterLifetime,
  isFighterDead,
  isFighterEvaded,
  isFighterFighting,
  isFighterLoading,
  isFighterReady,
} from '../fighterUtils';
import { fetchRaiderApiData, findStatByName } from './raiderUtils';

export type FighterStatsName = 'strength' | 'intelligence' | 'agility' | 'wisdom' | 'charm' | 'luck';

export type FighterAttributeName = 'strength' | 'intelligence' | 'agility' | 'wisdom' | 'charm' | 'luck' | 'level';

export type RaiderAttribute = {
  trait_type: string;
  value: string | number | undefined;
};

export type RaiderApiData = {
  name: string;
  description: string;
  image: string;
  attributes: Array<RaiderAttribute>;
};

export interface RaiderInit extends FighterInit, RaiderInterface {}

export interface RaiderLoading extends FighterLoading, RaiderInterface {
  name: string;
}

export interface RaiderReady extends FighterReady, RaiderInterface {
  name: string;
  stats: FighterStatsEssentials;
  build: FighterBuild;
  lifeTime: FighterLifetime;
}

export interface RaiderFighting extends FighterFighting, RaiderReady {}

export interface RaiderDead extends FighterDead, RaiderReady {}

export interface RaiderEvaded extends FighterEvaded, RaiderReady {}

export interface RaiderInterface extends FighterInterface {
  stats: FighterStatsEssentials | null;
  equip(
    gearA: Stats | null,
    gearB: Stats | null,
    knickknack: { name: KnickknackName } | null,
    finger: Stats | null
  ): Promise<FighterReady>;
  isEquiped(): boolean;
}

export class Raider implements RaiderInterface {
  status: FighterStatus = 'Init';
  raiderId: number;
  stats: FighterStatsEssentials | null = null;
  build: FighterBuild | null = null;
  lifeTime: FighterLifetime | null = null;
  name: string | null = null;
  isRaider(): boolean {
    return true;
  }
  constructor(raiderId: number) {
    this.name = raiderId.toString();
    this.raiderId = raiderId;
  }
  fromInitToLoading(): Promise<FighterLoading> {
    return new Promise<FighterLoading>((resolve) => {
      if (this.status !== 'Init') {
        throw new Error(`Transition from ${this.status} to Loading is impossible`);
      }
      log(`Raider #${this.raiderId}`, 'Loading', 'debug');
      this.status = 'Loading';
      fetchRaiderApiData(this.raiderId).then((raiderApiData: RaiderApiData) => {
        this.name = raiderApiData.name;
        this.stats = {
          stats: {
            strength: findStatByName(raiderApiData, 'strength').value as number,
            intelligence: findStatByName(raiderApiData, 'intelligence').value as number,
            agility: findStatByName(raiderApiData, 'agility').value as number,
            wisdom: findStatByName(raiderApiData, 'wisdom').value as number,
            charm: findStatByName(raiderApiData, 'charm').value as number,
            luck: findStatByName(raiderApiData, 'luck').value as number,
          },
          level: findStatByName(raiderApiData, 'level').value as number,
        };
        this.build = computeRaiderBuild(this.stats!);
        resolve(this as FighterLoading);
      });
    });
  }
  fromLoadingToReady(): Promise<FighterReady> {
    if (!isFighterLoading(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    if (this.build === null) {
      throw new Error(`Transition from Loading to Ready is impossible if #${this.raiderId}.build is empty.`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    log(`Raider #${this.raiderId}`, `Ready`, 'debug');
    log(`Raider #${this.raiderId}`, `Build = ${JSON.stringify(this.build)}`, 'debug');
    log(`Raider #${this.raiderId}`, `Lifetime = ${JSON.stringify(this.lifeTime)}`, 'debug');
    this.status = 'Ready';
    return Promise.resolve(this as FighterReady);
  }
  fromReadyToFighting(): Promise<FighterFighting> {
    if (!isFighterReady(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Fighting is impossible`);
    }
    this.status = 'Fighting';
    return Promise.resolve(this);
  }
  fromFightingToDead(): Promise<FighterDead> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Dead is impossible`);
    }
    log(`Raider #${this.raiderId}`, `is dead`, 'debug');
    this.lifeTime.health = 0;
    this.status = 'Dead';
    return Promise.resolve(this);
  }
  fromFightingToEvaded(): Promise<FighterEvaded> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Evaded is impossible`);
    }
    this.status = 'Evaded';
    return Promise.resolve(this);
  }
  fromFightingToReady(): Promise<FighterReady> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  fromDeadToReady(): Promise<FighterReady> {
    if (!isFighterDead(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  fromEvadedToReady(): Promise<FighterReady> {
    if (!isFighterEvaded(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  async hit(hit: HitInterface): Promise<FighterFighting | FighterEvaded | FighterDead> {
    if (!isFighterFighting(this)) {
      throw new Error(`Hit on raider status ${this.status} is not allowed`);
    }
    this.lifeTime.health -= hit.damageDealt;
    if (this.lifeTime.health <= 0) {
      return this.fromFightingToDead();
    }
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
    this.build = computeBuildWithAffects(this);
    await computeAffectDamage(this, hit);

    return Promise.resolve(this);
  }
  updateAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    for (let index = 0; index < this.lifeTime.affects.length; index++) {
      --this.lifeTime.affects[index].remainingTurns;
    }
    this.lifeTime.affects = this.lifeTime.affects.filter((affect) => affect.remainingTurns > 0);
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
    this.build = computeBuildWithAffects(this as FighterFighting);
  }
  resetAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    this.lifeTime.affects = [];
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
  }
  equip(
    gearA: Stats | null,
    gearB: Stats | null,
    knickknack: { name: KnickknackName } | null,
    finger: Stats | null
  ): Promise<FighterReady> {
    if (!isFighterReady(this) || !this.stats) {
      throw new Error(`Can't equip on a fighter which is not in Ready state`);
    }
    this.stats.mainHand = undefined;
    this.stats.dress = undefined;
    this.stats.knickknack = undefined;
    this.stats.finger = undefined;
    if (gearA) {
      this.stats.mainHand = gearA;
    }
    if (gearB) {
      this.stats.dress = gearB;
    }
    if (knickknack) {
      this.stats.knickknack = knickknack;
    }
    if (finger) {
      this.stats.finger = finger;
    }
    this.build = computeRaiderBuild(this.stats);
    this.lifeTime = createFighterLifetime(this.build);
    log(`Raider #${this.raiderId}`, `New equipment`, 'debug');
    log(`Raider #${this.raiderId}`, `Stats = ${JSON.stringify(this.stats)}`, 'debug');
    log(`Raider #${this.raiderId}`, `Build = ${JSON.stringify(this.build)}`, 'debug');
    log(`Raider #${this.raiderId}`, `Lifetime = ${JSON.stringify(this.lifeTime)}`, 'debug');
    return Promise.resolve(this as FighterReady);
  }
  isEquiped(): boolean {
    return Boolean(this.stats?.mainHand || this.stats?.dress);
  }
  reset(): Promise<FighterReady> {
    if (isFighterDead(this)) {
      return this.fromDeadToReady();
    }
    if (isFighterEvaded(this)) {
      return this.fromEvadedToReady();
    }
    if (isFighterFighting(this)) {
      return this.fromFightingToReady();
    }
    throw new Error(`Can't reset a fighter in ${this.status} state.`);
  }
}
