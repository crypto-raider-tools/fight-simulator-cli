import fetch from 'cross-fetch';
import { FighterAttributeName, RaiderApiData, RaiderAttribute } from './raider';

/**
 * Validate RaiderAttribute type.
 *
 * @param data
 * @returns boolean
 */
export function isRaiderAttribute(data: any): data is RaiderAttribute {
  if (typeof data?.trait_type !== 'string') {
    console.info(`Failed to parse attribute, trait_type is ${typeof data?.trait_type}, string expected.`);
    return false;
  }
  return true;
}

/**
 * Validate RaiderApiData type.
 *
 * @param data
 * @returns boolean
 */
export function isRaiderData(data: any): data is RaiderApiData {
  if (typeof data?.name !== 'string') {
    console.info(`Failed to parse raiderApiData, name is ${typeof data?.name}, string expected.`);
    return false;
  }
  if (typeof data?.description !== 'string') {
    console.info(`Failed to parse raiderApiData, description is ${typeof data?.description}, string expected.`);
    return false;
  }
  if (typeof data?.image !== 'string') {
    console.info(`Failed to parse raiderApiData, image is ${typeof data?.image}, string expected.`);
    return false;
  }
  if (typeof data?.attributes !== 'object') {
    console.info(`Failed to parse raiderApiData, attributes is ${typeof data?.attributes}, object expected.`);
    return false;
  }

  if (data.attributes.length > 0) {
    for (let index = 0; index < data.attributes.length; index++) {
      const attribute: any = data.attributes[index];
      if (!isRaiderAttribute(attribute)) {
        console.info(`Failed to parse raiderApiData attribute ${JSON.stringify(attribute)}.`);
        return false;
      }
    }
  }

  return true;
}

/**
 * Call to crypto raider API.
 *
 * @param raider
 * @param address
 * @returns Promise
 */
export function fetchRaiderApiData(address: number): Promise<RaiderApiData> {
  let url: string = `https://api.cryptoraiders.xyz/raider/${address}`;
  return fetch(url)
    .then((response) => response.json())
    .then((data) => {
      if (!isRaiderData(data)) {
        throw new Error(`${url} did not return a valid body.`);
      }
      return data;
    });
}

/**
 * Return the attribute from the name.
 *
 * @param raiderApiData
 * @param statName
 * @returns RaiderAttribute
 */
export function findStatByName(raiderApiData: RaiderApiData, statName: FighterAttributeName): RaiderAttribute {
  const attribute = raiderApiData.attributes.find((attribute) => attribute.trait_type.toLocaleLowerCase() === statName);
  if (!attribute) {
    throw new Error(`${statName} does not exist.`);
  }
  return attribute;
}
