import { HitInterface } from '../../hits/hit';
import { log } from '../../logger';
import {
  FighterDead,
  FighterEvaded,
  FighterFighting,
  FighterLifetime,
  FighterLoading,
  FighterReady,
  FighterStatus,
} from '../fighter';
import {
  computeBuildWithAffects,
  computeAffectDamage,
  createFighterLifetime,
  isFighterDead,
  isFighterEvaded,
  isFighterFighting,
  isFighterLoading,
  isFighterReady,
} from '../fighterUtils';
import { RaiderFighting, RaiderInterface } from './raider';
import {
  computeRaiderBuild,
  FighterBuild,
  FighterStatsEssentials,
  KnickknackName,
  Stats,
} from '@crypto-raider-tools/computation';

export class MockedRaider implements RaiderInterface {
  status: FighterStatus = 'Init';
  stats: FighterStatsEssentials | null = null;
  build: FighterBuild | null = null;
  lifeTime: FighterLifetime | null = null;
  name: string;
  input: Array<string>;
  isRaider(): boolean {
    return true;
  }
  constructor(input: Array<string>) {
    this.input = input;
    this.name = input.join(',');
    if (input.length != 7) {
      throw new Error(`Can't create a mocked raider with this input: "${this.name}"`);
    }
  }
  fromInitToLoading(): Promise<FighterLoading> {
    if (this.status !== 'Init') {
      throw new Error(`Transition from ${this.status} to Loading is impossible`);
    }
    log(`MockRaider (${this.name})`, 'Loading', 'debug');
    this.status = 'Loading';
    this.stats = {
      stats: {
        strength: parseInt(this.input[0]),
        intelligence: parseInt(this.input[1]),
        agility: parseInt(this.input[2]),
        wisdom: parseInt(this.input[3]),
        charm: parseInt(this.input[4]),
        luck: parseInt(this.input[5]),
      },
      level: parseInt(this.input[6]),
    };
    this.build = computeRaiderBuild(this.stats);
    return Promise.resolve(this);
  }
  fromLoadingToReady(): Promise<FighterReady> {
    if (!isFighterLoading(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    if (this.build === null) {
      throw new Error(`Transition from Loading to Ready is impossible if (${this.name}).build is empty.`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    log(`MockRaider (${this.name})`, `Ready`, 'debug');
    log(`MockRaider (${this.name})`, `Build = ${JSON.stringify(this.build)}`, 'debug');
    log(`MockRaider (${this.name})`, `Lifetime = ${JSON.stringify(this.lifeTime)}`, 'debug');
    this.status = 'Ready';
    return Promise.resolve(this as FighterReady);
  }
  fromReadyToFighting(): Promise<FighterFighting> {
    if (!isFighterReady(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Fighting is impossible`);
    }
    this.status = 'Fighting';
    return Promise.resolve(this as FighterFighting);
  }
  fromFightingToDead(): Promise<FighterDead> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Dead is impossible`);
    }
    log(`MockRaider (${this.name})`, `is dead`, 'debug');
    this.lifeTime.health = 0;
    this.status = 'Dead';
    return Promise.resolve(this as FighterDead);
  }
  fromFightingToEvaded(): Promise<FighterEvaded> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Evaded is impossible`);
    }
    return Promise.resolve(this as FighterEvaded);
  }
  fromFightingToReady(): Promise<FighterReady> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this as FighterReady);
  }
  fromDeadToReady(): Promise<FighterReady> {
    if (!isFighterDead(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this as FighterReady);
  }
  fromEvadedToReady(): Promise<FighterReady> {
    if (!isFighterEvaded(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this as FighterReady);
  }
  async hit(hit: HitInterface): Promise<FighterFighting | FighterEvaded | FighterDead> {
    if (!isFighterFighting(this)) {
      throw new Error(`Hit on raider status ${this.status} is not allowed`);
    }
    this.lifeTime.health -= hit.damageDealt;
    if (this.lifeTime.health <= 0) {
      return this.fromFightingToDead();
    }
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
    this.build = computeBuildWithAffects(this);
    await computeAffectDamage(this, hit);

    return Promise.resolve(this as FighterFighting | FighterEvaded);
  }
  updateAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    for (let index = 0; index < this.lifeTime.affects.length; index++) {
      --this.lifeTime.affects[index].remainingTurns;
    }
    this.lifeTime.affects = this.lifeTime.affects.filter((affect) => affect.remainingTurns > 0);
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
    this.build = computeBuildWithAffects(this as FighterFighting);
  }
  resetAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    this.lifeTime.affects = [];
    this.build = computeRaiderBuild((this as RaiderFighting).stats);
  }
  equip(
    mainHand: Stats | null,
    dress: Stats | null,
    knickknack: { name: KnickknackName } | null,
    finger: Stats | null
  ): Promise<FighterReady> {
    if (!isFighterReady(this) || !this.stats) {
      throw new Error(`Can't equip on a fighter which is not in Ready state`);
    }
    this.stats.mainHand = undefined;
    this.stats.dress = undefined;
    this.stats.knickknack = undefined;
    this.stats.finger = undefined;
    if (mainHand) {
      this.stats.mainHand = mainHand;
    }
    if (dress) {
      this.stats.dress = dress;
    }
    if (knickknack) {
      this.stats.knickknack = knickknack;
    }
    if (finger) {
      this.stats.finger = finger;
    }
    this.build = computeRaiderBuild(this.stats);
    this.lifeTime = createFighterLifetime(this.build);
    log(`MockRaider (${this.name})`, `New equipment`, 'debug');
    log(`MockRaider (${this.name})`, `Stats = ${JSON.stringify(this.stats)}`, 'debug');
    log(`MockRaider (${this.name})`, `Build = ${JSON.stringify(this.build)}`, 'debug');
    log(`MockRaider (${this.name})`, `Lifetime = ${JSON.stringify(this.lifeTime)}`, 'debug');
    return Promise.resolve(this as FighterReady);
  }
  isEquiped(): boolean {
    return Boolean(this.stats?.mainHand || this.stats?.dress);
  }
  reset(): Promise<FighterReady> {
    if (isFighterDead(this)) {
      return this.fromDeadToReady();
    }
    if (isFighterEvaded(this)) {
      return this.fromEvadedToReady();
    }
    if (isFighterFighting(this)) {
      return this.fromFightingToReady();
    }
    throw new Error(`Can't reset a fighter in ${this.status} state.`);
  }
}
