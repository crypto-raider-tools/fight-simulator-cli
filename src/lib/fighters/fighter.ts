export type FighterStatus = 'Init' | 'Loading' | 'Ready' | 'Fighting' | 'Dead' | 'Evaded';
import { FighterBuild, KnickknackName } from '@crypto-raider-tools/computation';
import { HitInterface } from '../hits/hit';
import { Raider } from './raider/raider';

export type FighterBuildName =
  | 'MaxHP'
  | 'MinDamage'
  | 'MaxDamage'
  | 'HitChance'
  | 'FirstHit'
  | 'MeleeCrit'
  | 'CritDamageMultiplier'
  | 'CritResist'
  | 'EvadeChance'
  | 'MeleeResist';

export type Affect = {
  remainingTurns: number;
  affectName: KnickknackName;
  affectBuild: Partial<FighterBuild>;
};

export type FighterLifetime = {
  health: number;
  affects: Array<Affect>;
};

export interface FighterInterface {
  status: FighterStatus;
  build: FighterBuild | null;
  lifeTime: FighterLifetime | null;
  name: string | null;
  isRaider(): this is Raider;
  fromInitToLoading(): Promise<FighterLoading>;
  fromLoadingToReady(): Promise<FighterReady>;
  fromReadyToFighting(): Promise<FighterFighting>;
  fromFightingToDead(): Promise<FighterDead>;
  fromFightingToEvaded(): Promise<FighterEvaded>;
  fromFightingToReady(): Promise<FighterReady>;
  fromDeadToReady(): Promise<FighterReady>;
  fromEvadedToReady(): Promise<FighterReady>;
  reset(): Promise<FighterReady>;
  updateAffects(): void;
  resetAffects(): void;
  hit(hit: HitInterface): Promise<FighterFighting | FighterEvaded | FighterDead>;
}

export interface FighterInit extends FighterInterface {}

export interface FighterLoading extends FighterInit {
  name: string;
}

export interface FighterReady extends FighterLoading {
  build: FighterBuild;
  lifeTime: FighterLifetime;
}

export interface FighterFighting extends FighterReady {}

export interface FighterDead extends FighterReady {}

export interface FighterEvaded extends FighterReady {}
