import { FighterBuild, MobBuildName, mobBuilds } from '@crypto-raider-tools/computation';
import { HitInterface } from '../../hits/hit';
import { log } from '../../logger';
import {
  FighterDead,
  FighterEvaded,
  FighterFighting,
  FighterInterface,
  FighterLifetime,
  FighterLoading,
  FighterReady,
  FighterStatus,
} from '../fighter';
import {
  computeBuildWithAffects,
  computeAffectDamage,
  createFighterLifetime,
  isFighterDead,
  isFighterEvaded,
  isFighterFighting,
  isFighterReady,
} from '../fighterUtils';

export class Mob implements FighterInterface {
  status: FighterStatus = 'Init';
  name: MobBuildName;
  build: FighterBuild | null = null;
  lifeTime: FighterLifetime | null = null;
  isRaider(): boolean {
    return false;
  }
  constructor(name: MobBuildName) {
    this.name = name;
  }
  fromInitToLoading(): Promise<FighterLoading> {
    if (this.status !== 'Init') {
      throw new Error(`Transition from ${this.status} to Loading is impossible`);
    }
    log(`Mob ${this.name}`, `Loading`, 'debug');
    this.status = 'Loading';
    return Promise.resolve(this);
  }
  fromLoadingToReady(): Promise<FighterReady> {
    if (this.status !== 'Loading') {
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.build = mobBuilds[this.name];
    if (this.build === null) {
      throw new Error(`Transition from Loading to Ready is impossible if ${this.name}.build is empty.`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    log(`Mob ${this.name}`, `Build = ${JSON.stringify(this.build)}`, 'debug');
    log(`Mob ${this.name}`, `Ready`, 'debug');
    log(`Mob ${this.name}`, `Lifetime = ${JSON.stringify(this.lifeTime)}`, 'debug');
    this.status = 'Ready';
    if (!isFighterReady(this)) {
      throw new Error(`Transition to Ready failed`);
    }
    return Promise.resolve(this);
  }
  fromReadyToFighting(): Promise<FighterFighting> {
    if (!isFighterReady(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Fighting is impossible`);
    }
    this.status = 'Fighting';
    return Promise.resolve(this);
  }
  fromFightingToDead(): Promise<FighterDead> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Dead is impossible`);
    }
    this.lifeTime.health = 0;
    log(`Mob ${this.name}`, `is dead`, 'debug');
    this.status = 'Dead';
    return Promise.resolve(this);
  }
  fromFightingToEvaded(): Promise<FighterEvaded> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Dead is impossible`);
    }
    log(`Mob ${this.name}`, `is dead`, 'debug');
    this.status = 'Evaded';
    return Promise.resolve(this);
  }
  fromFightingToReady(): Promise<FighterReady> {
    if (!isFighterFighting(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  fromDeadToReady(): Promise<FighterReady> {
    if (!isFighterDead(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  fromEvadedToReady(): Promise<FighterReady> {
    if (!isFighterEvaded(this)) {
      // @ts-ignore
      throw new Error(`Transition from ${this.status} to Ready is impossible`);
    }
    this.lifeTime = createFighterLifetime(this.build);
    this.status = 'Ready';
    return Promise.resolve(this);
  }
  async hit(hit: HitInterface): Promise<FighterFighting | FighterEvaded | FighterDead> {
    if (!isFighterFighting(this)) {
      throw new Error(`Hit on mob status ${this.status} is not allowed`);
    }
    this.lifeTime.health -= hit.damageDealt;
    if (this.lifeTime.health <= 0) {
      return this.fromFightingToDead();
    }
    this.build = mobBuilds[this.name];
    this.build = computeBuildWithAffects(this);
    await computeAffectDamage(this, hit);

    return Promise.resolve(this as FighterFighting);
  }
  updateAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    for (let index = 0; index < this.lifeTime.affects.length; index++) {
      --this.lifeTime.affects[index].remainingTurns;
    }
    this.lifeTime.affects = this.lifeTime.affects.filter((affect) => affect.remainingTurns > 0);
    this.build = mobBuilds[this.name];
    this.build = computeBuildWithAffects(this as FighterFighting);
  }
  resetAffects(): void {
    if (!this.lifeTime?.affects) {
      throw new Error(`Can't update affect on fighter without lifetime.`);
    }
    this.lifeTime.affects = [];
    this.build = mobBuilds[this.name];
  }
  reset(): Promise<FighterReady> {
    if (isFighterDead(this)) {
      return this.fromDeadToReady();
    }
    if (isFighterEvaded(this)) {
      return this.fromEvadedToReady();
    }
    if (isFighterFighting(this)) {
      return this.fromFightingToReady();
    }
    throw new Error(`Can't reset a fighter in ${this.status} state.`);
  }
}
