import { FighterBuild, isMobBuildName } from '@crypto-raider-tools/computation';
import { HitInterface } from '../hits/hit';
import { log } from '../logger';
import colors from 'colors/safe.js';
import {
  FighterDead,
  FighterEvaded,
  FighterFighting,
  FighterInit,
  FighterInterface,
  FighterLifetime,
  FighterLoading,
  FighterReady,
} from './fighter';
import { Mob } from './mob/mob';
import { MockedRaider } from './raider/mockRaider';
import { Raider } from './raider/raider';

export function computeBuildWithAffects(fighter: FighterReady): FighterBuild {
  let newBuild = { ...fighter.build };
  for (let index = 0; index < fighter.lifeTime.affects.length; index++) {
    const affect = fighter.lifeTime.affects[index];
    const affectBuild = affect.affectBuild;
    newBuild = {
      maxHP: affectBuild?.maxHP ? affectBuild?.maxHP : fighter.build.maxHP,
      minDamage: affectBuild?.minDamage ? affectBuild?.minDamage : fighter.build.minDamage,
      maxDamage: affectBuild?.maxDamage ? affectBuild?.maxDamage : fighter.build.maxDamage,
      hitChance: affectBuild?.hitChance ? affectBuild?.hitChance : fighter.build.hitChance,
      hitFirst: affectBuild?.hitFirst ? affectBuild?.hitFirst : fighter.build.hitFirst,
      meleeCrit: affectBuild?.meleeCrit ? affectBuild?.meleeCrit : fighter.build.meleeCrit,
      critDamageMultiplier: affectBuild?.critDamageMultiplier
        ? affectBuild?.critDamageMultiplier
        : fighter.build.critDamageMultiplier,
      critResist: affectBuild?.critResist ? affectBuild?.critResist : fighter.build.critResist,
      evadeChance: affectBuild?.evadeChance ? affectBuild?.evadeChance : fighter.build.evadeChance,
      meleeResist: affectBuild?.meleeResist ? affectBuild?.meleeResist : fighter.build.meleeResist,
    };
  }
  return newBuild;
}

export async function computeAffectDamage(fighter: FighterFighting, hit: HitInterface): Promise<void> {
  for (let index = 0; index < fighter.lifeTime.affects.length; index++) {
    const affect = fighter.lifeTime.affects[index];
    const affectName = affect.affectName;
    if (affectName === 'Thorns') {
      let damageReflect = hit.damageDealt * 0.45;
      log(
        'Hit',
        `${fighter.name} reflect ${colors.red(damageReflect.toFixed())} dmg with ${colors.yellow(affectName)}!`,
        'debug'
      );
      hit.from.lifeTime.health -= damageReflect;
      if (hit.from.lifeTime.health <= 0) {
        await hit.from.fromFightingToDead();
        return Promise.resolve();
      }
    }
    if (affectName === 'Enflame') {
      let damageReflect = fighter.build.maxHP * 0.05;
      log(
        'Hit',
        `${fighter.name} receive ${colors.red(damageReflect.toFixed())} damage from ${colors.yellow(affectName)}!`,
        'debug'
      );
      fighter.lifeTime.health -= damageReflect;
      if (fighter.lifeTime.health <= 0) {
        await fighter.fromFightingToDead();
        return Promise.resolve();
      }
    }
  }
  return Promise.resolve();
}

export function fromStringToFighter(input: string): FighterInterface {
  if (/^\d+,\d+,\d+,\d+,\d+,\d+,\d+$/.test(input)) {
    return new MockedRaider(input.split(','));
  }
  if (input === 'krok') {
    return new Mob('rat');
  }
  if (input === 'krokHeroic') {
    return new Mob('ratHeroic');
  }
  if (isMobBuildName(input)) {
    return new Mob(input);
  }
  const raiderId = parseInt(input);
  if (raiderId > 0) {
    return new Raider(raiderId);
  }
  throw new Error(`${input} is not a valid input, number or mob name expected.`);
}

export function isFighterInit(fighter: FighterInterface): fighter is FighterInit {
  return fighter.status === 'Init';
}
export function isFighterLoading(fighter: FighterInterface): fighter is FighterLoading {
  return fighter.status === 'Loading';
}
export function isFighterReady(fighter: FighterInterface): fighter is FighterReady {
  return fighter.status === 'Ready';
}
export function isFighterFighting(fighter: FighterInterface): fighter is FighterFighting {
  return fighter.status === 'Fighting';
}
export function isFighterDead(fighter: FighterInterface): fighter is FighterDead {
  return fighter.status === 'Dead';
}
export function isFighterEvaded(fighter: FighterInterface): fighter is FighterEvaded {
  return fighter.status === 'Evaded';
}

export function createFighterLifetime(build: FighterBuild): FighterLifetime {
  return {
    health: build.maxHP,
    affects: [],
  };
}
