import colors from 'colors/safe.js';

export type LogLevel = 'debug' | 'info';

let debugLogIsEnable = true;
let debugIsEnable = true;

export function displayDebugLog(displayDebugLog: boolean): void {
  debugLogIsEnable = displayDebugLog;
}

export function displayAllLog(displayAllLog: boolean): void {
  debugIsEnable = displayAllLog;
}

export function log(type: string, message: string, level: LogLevel) {
  if (!debugIsEnable) return;
  if (level === 'debug' && !debugLogIsEnable) {
    return;
  }
  console.log(`[${colors.grey(new Date().toISOString().padEnd(25, ' '))}][${type.padEnd(15, ' ')}] ${message}`);
}
