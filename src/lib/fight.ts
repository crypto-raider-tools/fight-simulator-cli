import { isMobBuildName, MobBuildName } from '@crypto-raider-tools/computation';
import colors from 'colors/safe.js';
import { FighterFighting, FighterInterface, FighterReady } from './fighters/fighter';
import { isFighterDead, isFighterFighting, isFighterReady } from './fighters/fighterUtils';
import { Mob } from './fighters/mob/mob';
import { log } from './logger';
import { Round } from './round';

export type FightLifetimeState = 'Init' | 'Fighting' | 'Done';

export interface FightInterface {
  fighterA: FighterInterface;
  fighterB: FighterInterface;
  rounds: Map<number, Round>;
  currentState: FightLifetimeState;
  simulate(): Promise<boolean>;
  fromInitToFighting(): Promise<boolean>;
  fromFightingToDone(): boolean;
  hasFighterAWon(): boolean;
}

function whoFightFirst(fighterA: FighterReady | FighterFighting, fighterB: FighterReady | FighterFighting): boolean {
  const total = fighterA.build.hitFirst + fighterB.build.hitFirst;
  const random = Math.random() % total;
  if (random > fighterA.build.hitFirst) {
    return false;
  }
  return true;
}

export const mobRevivalMap: Partial<Record<MobBuildName, MobBuildName>> = {
  rat: 'krok',
  ratHeroic: 'krokHeroic',
};

export class Fight implements FightInterface {
  fighterA: FighterInterface;
  fighterB: FighterInterface;
  rounds: Map<number, Round> = new Map();
  currentState: FightLifetimeState = 'Init';
  fighterAWon: boolean = false;
  constructor(fighterA: FighterInterface, fighterB: FighterInterface) {
    log('Fight', 'Init', 'debug');
    this.fighterA = fighterA;
    this.fighterB = fighterB;
  }
  async simulate(): Promise<boolean> {
    await this.fromInitToFighting();
    return Promise.resolve(this.hasFighterAWon());
  }
  async fromInitToFighting(): Promise<boolean> {
    if (this.currentState !== 'Init') {
      throw new Error(`Transition from ${this.currentState} to Fighting is impossible`);
    }
    if (!isFighterReady(this.fighterA) || !isFighterReady(this.fighterB)) {
      throw new Error(`Transition from Init to Fighting is impossible if fighters are not Ready.`);
    }
    log('Fight', colors.yellow('Fighting'), 'debug');
    this.currentState = 'Fighting';

    await this.fighterA.fromReadyToFighting();
    await this.fighterB.fromReadyToFighting();

    let roundCount = 0;
    while (this.fighterA.status === 'Fighting' && this.fighterB.status === 'Fighting') {
      if (!isFighterFighting(this.fighterA) || !isFighterFighting(this.fighterB)) {
        throw new Error(`Transition from Init to Fighting is impossible if fighters are not Ready.`);
      }
      roundCount++;
      let currentRound = null;
      log(`Fight`, `New round`, 'debug');
      this.fighterA.updateAffects();
      this.fighterB.updateAffects();
      if (whoFightFirst(this.fighterA, this.fighterB)) {
        currentRound = new Round(this.fighterA, this.fighterB, roundCount);
      } else {
        currentRound = new Round(this.fighterB, this.fighterA, roundCount);
      }

      this.rounds.set(roundCount, currentRound);
      currentRound.simulate();

      if (
        isMobBuildName(this.fighterB.name) &&
        Object.keys(mobRevivalMap).includes(this.fighterB.name) &&
        isFighterDead(this.fighterB)
      ) {
        log(`Fight`, `${this.fighterA.name} win over ${this.fighterB.name} in ${this.rounds.size} round(s).`, 'debug');
        // @ts-expect-error
        this.fighterB = new Mob(mobRevivalMap[this.fighterB.name]);
        await this.fighterB.fromInitToLoading();
        await this.fighterB.fromLoadingToReady();
        await this.fighterB.fromReadyToFighting();
        roundCount = 0;
        this.fighterA.resetAffects();
        this.fighterA.lifeTime.health = Math.floor(
          this.fighterA.lifeTime.health + this.fighterA.build.maxHP * ((15 + 10 * Math.random()) / 100)
        );
        if (this.fighterA.lifeTime.health > this.fighterA.build.maxHP) {
          this.fighterA.lifeTime.health = this.fighterA.build.maxHP;
        }
      }

      if (
        isMobBuildName(this.fighterA.name) &&
        Object.keys(mobRevivalMap).includes(this.fighterA.name) &&
        isFighterDead(this.fighterA)
      ) {
        log(`Fight`, `${this.fighterB.name} win over ${this.fighterA.name} in ${this.rounds.size} round(s).`, 'debug');
        // @ts-expect-error
        this.fighterA = new Mob(mobRevivalMap[this.fighterA.name]);
        await this.fighterA.fromInitToLoading();
        await this.fighterA.fromLoadingToReady();
        await this.fighterA.fromReadyToFighting();
        roundCount = 0;
        this.fighterB.resetAffects();

        (this.fighterB as FighterFighting).lifeTime.health = Math.floor(
          (this.fighterB as FighterFighting).lifeTime.health +
            (this.fighterB as FighterFighting).build.maxHP * ((15 + 10 * Math.random()) / 100)
        );
        if ((this.fighterB as FighterFighting).lifeTime.health > (this.fighterB as FighterFighting).build.maxHP) {
          (this.fighterB as FighterFighting).lifeTime.health = (this.fighterB as FighterFighting).build.maxHP;
        }
      }
    }

    return Promise.resolve(this.fromFightingToDone());
  }
  fromFightingToDone(): boolean {
    if (this.currentState !== 'Fighting') {
      throw new Error(`Transition from ${this.currentState} to Done is impossible`);
    }
    this.fighterAWon = isFighterDead(this.fighterB);
    this.currentState = 'Done';
    log(
      `Fight`,
      `${this.fighterAWon ? this.fighterA.name : this.fighterB.name} win over ${
        !this.fighterAWon ? this.fighterA.name : this.fighterB.name
      } in ${this.rounds.size} round(s).`,
      'debug'
    );
    return true;
  }
  hasFighterAWon(): boolean {
    if (this.currentState !== 'Done') {
      throw new Error(`Can't get result on fight with ${this.currentState} state`);
    }
    return this.fighterAWon;
  }
}
