FROM node:16

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . .

RUN npm run all

RUN /app/bin/fight-simulator-cli --help

EXPOSE 3000

ENTRYPOINT [ "/app/bin/fight-simulator-cli", "serve" ]
